/**
 * Created by Amir on 1/8/2017.
 */

FlowRouter.route('/match', {name: 'Round Page',
    action: function(params, queryParams) {
        Meteor.call('changeGameStateForMatch', "matchList",queryParams);
    }
});


FlowRouter.route('/list', {name: 'List Page',
    action: function(params, queryParams) {
        console.log("route working 0");
        Meteor.call('changeGameState', "groupList");
    }
});


FlowRouter.route('/admin', {name: 'Admin Page',
    action: function(params, queryParams) {
        Meteor.call('changeGameState', "adminList");
    }
});

FlowRouter.route('/', {name: 'Main Page',
    action: function(params, queryParams) {

    }
});
