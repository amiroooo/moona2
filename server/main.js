import { Meteor } from 'meteor/meteor';
var bodyParser = Npm.require('body-parser');
Picker.middleware(bodyParser.urlencoded({ extended: false }));
Picker.middleware(bodyParser.json());

Meteor.startup(function () {
//Groups.remove({});
//Rounds.remove({});
//RoundParts.remove({});
    if (!Groups.findOne()) {
        /*
        Groups.insert({groupName: "legendary team", points : 20});
        Groups.insert({groupName: "dragonborn", points : 32});
        Groups.insert({groupName: "quadcopter", points : 2});
        Groups.insert({groupName: "sucide squad", points : 67});
        Groups.insert({groupName: "power puffs", points : 23});
        */
    }
    //Rounds.remove({});
    if (!Rounds.findOne()) {
        //Rounds.insert({leftSide: "legendary team", rightSide : "dragonborn"});
        //Rounds.insert({leftSide: "dragonborn", rightSide : "sucide squad"});
        //Rounds.insert({leftSide: "quadcopter", rightSide : "amir"});
        //Rounds.insert({leftSide: "sucide squad", rightSide : "majd"});
        //Rounds.insert({leftSide: "power puffs", rightSide : "dragonborn"});

    }
    if (!Obstacles.findOne()) {
        //Obstacles.insert({name: "none", description:"none", owner:"none", maxPoints:0, tickPoints:0});
    }

    if (!FinalRanks.findOne()) {
        //FinalRanks.insert({first: "none", second:"none", third:"none"});
    }

    if (!RefreshPages.findOne()) {
        //RefreshPages.insert({auto:true});
    }

    /*
    if (!RoundsOrder.findOne()) {
     RoundsOrder.remove({});
     Rounds.remove({ });
        var stage1 = RoundsOrder.findOne({stage: 1});

        RoundsOrder.update(stage1._id,{$set:{name:"Stage 1"}});

        var stage2 = RoundsOrder.findOne({stage: 2});
        RoundsOrder.update(stage2._id,{$set:{name:"Stage 2"}});

        var stage3 = RoundsOrder.findOne({stage: 3});
        RoundsOrder.update(stage3._id,{$set:{name:"Playoffs"}});

        RoundsOrder.insert({stage: 1, order:[]});
        RoundsOrder.insert({stage: 2, order:[]});
        RoundsOrder.insert({stage: 3, order:[]});
    }
    */

    //var gamestate = GameState.findOne();
    //console.log(JSON.stringify(gamestate));

    // just to log last page that was on
    if (GameState.findOne()) {
        //GameState.remove({});
        s = GameState.find({}).fetch();
        //console.log("init state: " + s[0].state);
    }

    if (Groups.findOne()) {
        //Groups.remove({});
        s = Groups.find({}).fetch();
        //console.log("group 0: " + s[0].groupName);
    }

    // in case no pages in db, will create new value with default value
    if (!GameState.findOne()) {
        GameState.insert({state:"groupList", gameStage:1});
    }

    //SideData.remove({});

    //t = SideData.findOne();
    //if (t) {
        //Groups.remove({});
        //console.log("sideData is available");

        //console.log("left old points: "+t.leftSide.oldPoints);
        //console.log("right old points: "+t.rightSide.oldPoints);
    //}
    //SideData.remove({});
    if (!SideData.findOne()) {
        SideData.insert({leftSide:{groupName:"team_test1"},rightSide:{groupName:"team_test3"},roundTime:800000, action:0,isDone:0});
    }

    if (!AndroidEvents.findOne()) {
        AndroidEvents.insert({action:"init",data:{}});
    }

});


Meteor.methods({
    addGroup: function (groupData) {
        var groupCount = Groups.find({groupName: groupData.groupName}).count();
        if (groupCount == 0) {
            var groupID = Groups.insert(groupData);
            return groupID;
        }
    },
    switchRefreshTables: function () {
        var t = RefreshPages.findOne({});
        var isAuto = true;
        if (t){
           if (t.auto) {
               isAuto = false;
           }else{
               isAuto = true;
           }
            RefreshPages.update(t._id,{$set : {auto:isAuto}});
        }
        return isAuto;
    },
    addGroupPoints: function(groupData) {
        var groupCount = Groups.find({groupName:groupData.groupName}).count();
        if (groupCount == 1 && groupData.points.valueOf() >-100){
            var tempGroup = Groups.findOne({groupName: groupData.groupName});
            var newPoints = Number(groupData.points)+Number(tempGroup.points);
            Groups.update(tempGroup._id, {$set: {points: newPoints}});
            var mySideData = SideData.find({}).fetch();
            leftSide = mySideData[0].leftSide;
            rightSide = mySideData[0].rightSide;

            //console.log("leftSide.groupName:"+JSON.stringify(leftSide));
            //console.log("groupData.groupName:"+groupData.groupName);
            if (leftSide.groupName == groupData.groupName){
                leftSide.newPoints += Number(groupData.points);
                leftSide.obs1Points=mySideData[0].leftSide.obs1Points;
                leftSide.obs2Points=mySideData[0].leftSide.obs2Points;

            }else if(rightSide.groupName == groupData.groupName){
               rightSide.newPoints += Number(groupData.points);
                rightSide.obs1Points=mySideData[0].rightSide.obs1Points;
                rightSide.obs2Points=mySideData[0].rightSide.obs2Points;
            }
            SideData.update(mySideData[0]._id, {$set: {"leftSide":leftSide,"rightSide":rightSide, action:6}});
            return tempGroup;
        }
    },
    getServerTime: function(){
        return Date.now();
    },
    changeGameStateForMatch: function(newState,data){
        var myGameState = GameState.findOne({});
        GameState.update(myGameState._id, {$set: {state: newState}});
        var mySideData = SideData.findOne({});
        if (data != null){
            SideData.update(mySideData._id, {$set: {leftSide:{groupName:data.leftGroup,newPoints : 0},rightSide:{groupName:data.rightGroup,newPoints : 0, action:0}}});
        }
    },
    setRoundStateDone: function(id){
        Rounds.update(id, {$set: {state:4,isDone:4}});
        var mySideData = SideData.findOne({});
        SideData.update(mySideData._id, {$set: {state:4, action:4,isDone:4}});
        var andData = {action:"statusDone",data:id};
        updateAndroid(andData);
    },
    setPlayoffRoundStateDone: function(id){
        Rounds.update(id, {$set: {isDone:4}});
        var mySideData = SideData.findOne({});
        SideData.update(mySideData._id, {$set: { action:4,isDone:4}});
        var andData = {action:"statusDone",data:id};
        updateAndroid(andData);
    },
    setRoundStateReview: function(id){
        Rounds.update(id, {$set: {state:3,isDone:3}});
        var mySideData = SideData.findOne({});
        SideData.update(mySideData._id, {$set: {state:3, action:3,isDone:3}});
        var andData = {action:"statusReview",data:id};
        updateAndroid(andData);

    },
    setPlayoffRoundStateReview: function(id){
        Rounds.update(id, {$set: {isDone:3}});
        var mySideData = SideData.findOne({});
        SideData.update(mySideData._id, {$set: { action:3,isDone:3}});
        var andData = {action:"statusReview",data:id};
        updateAndroid(andData);

    },

    changeGameState: function(newState){
        var myGameState = GameState.findOne({});
        GameState.update(myGameState._id, {$set: {state: newState}});
    },
    changeGameStage: function(stage){
        var myGameState = GameState.findOne({});
        GameState.update(myGameState._id, {$set: {gameStage: stage}});
        var sideData = SideData.findOne({});
        SideData.update(sideData._id,{$set:{roundId:"0"}});
    },


    startGame: function(id){

        var mySideData = SideData.findOne({});
        var round = Rounds.findOne({_id:id});
        var doesntHaveObstacles = ((round.obstacle1 == "none" && round.obstacle2 == "none") || (round.obstacle3 == "none" && round.obstacle4 == "none"))
        if (round != undefined && round.state == 1 && !doesntHaveObstacles) {

            var obs1 = Obstacles.findOne({name: round.obstacle1});
            var obs2 = Obstacles.findOne({name: round.obstacle2});
            var obs3 = Obstacles.findOne({name: round.obstacle3});
            var obs4 = Obstacles.findOne({name: round.obstacle4});

            var obsTypeOn = 0;
            if (mySideData.roundId != round._id) {
                Meteor.call('setRoundStateDone', mySideData.roundId);
            }
            var myGameState = GameState.findOne({});
            GameState.update(myGameState._id, {$set: {state: "groupList"}});

            Meteor.setTimeout(function(){
                GameState.update(myGameState._id, {$set: {state: "matchList"}});
            },500);

            if (obs1 == undefined) {
                obs1 = {name: "none", description: "none", owner: "none", maxPoints: 0, tickPoints: 0};
                obsTypeOn = 1;
            }else{
                var groupData = {groupName:obs1.owner,points:obs1.maxPoints};
                if (obs1.owner != "none"){
                    Meteor.call('addGroupPoints', groupData);
                }
            }
            var maxObs1 = Number(obs1.maxPoints);
            if (isNaN(maxObs1)) {
                maxObs1 = 0;
            }

            if (obs2 == undefined) {
                obs2 = {name: "none", description: "none", owner: "none", maxPoints: 0, tickPoints: 0};
                obsTypeOn = obsTypeOn+10;
            }else{
                var groupData = {groupName:obs2.owner,points:obs2.maxPoints};
                if (obs2.owner != "none"){
                    Meteor.call('addGroupPoints', groupData);
                }
            }

            var maxObs2 = Number(obs2.maxPoints);
            if (isNaN(maxObs2)) {
                maxObs2 = 0;
            }


            if (obs3 == undefined) {
                obs3 = {name: "none", description: "none", owner: "none", maxPoints: 0, tickPoints: 0};
                obsTypeOn = obsTypeOn+100;
            }else{
                var groupData = {groupName:obs3.owner,points:obs3.maxPoints};
                if (obs3.owner != "none"){
                    Meteor.call('addGroupPoints', groupData);
                }
            }

            var maxObs3 = Number(obs3.maxPoints);
            if (isNaN(maxObs3)) {
                maxObs3 = 0;
            }



            if (obs4 == undefined) {
                obs4 = {name: "none", description: "none", owner: "none", maxPoints: 0, tickPoints: 0};
                obsTypeOn = obsTypeOn+1000;
            }else{
                var groupData = {groupName:obs4.owner,points:obs4.maxPoints};
                if (obs4.owner != "none"){
                    Meteor.call('addGroupPoints', groupData);
                }
            }

            var maxObs4 = Number(obs4.maxPoints);
            if (isNaN(maxObs4)) {
                maxObs4 = 0;
            }


            if (round != null) {
                SideData.update(mySideData._id, {
                    $set: {
                        action:1,
                        roundId: id, roundTime: round.timeLeft,state:2,isDone:2,
                        leftSide: {
                            groupName: round.leftSide,
                            newPoints: 0,
                            obs1Points: 0,
                            obs2Points: 0,
                            bonus: 0
                        },
                        rightSide: {
                            groupName: round.rightSide,
                            newPoints: 0,
                            obs1Points: 0,
                            obs2Points: 0,
                            bonus: 0
                        },
                        obs1Max: maxObs1, obs1Owner: obs1.owner,
                        obs2Max: maxObs2, obs2Owner: obs2.owner,
                        obs3Max: maxObs3, obs3Owner: obs3.owner,
                        obs4Max: maxObs4, obs4Owner: obs4.owner
                    }
                });
            }

            Rounds.update(round._id,{$set:{isDone:2,state:2,executeTime: Date.now()+round.timeLeft*60*1000}});

            var andData = {action:"startRound",data:{_id:id,timeLeft:round.timeLeft,obsType:obsTypeOn,obs1tick:obs1.tickPoints,obs2tick:obs2.tickPoints,obs3tick:obs3.tickPoints,obs4tick:obs4.tickPoints}};
            updateAndroid(andData);
            return round.timeLeft;


        }else if (round != undefined && round.state >=10 && !doesntHaveObstacles){

            var myGameState = GameState.findOne({});
            GameState.update(myGameState._id, {$set: {state: "playoffMatchList"}});
            var obs1 = Obstacles.findOne({name: round.obstacle1});
            var obsTypeOn = 0;
            if (obs1 == undefined) {
                obs1 = {name: "none", description: "none", owner: "none", maxPoints: 0, tickPoints: 0};
                obsTypeOn = 1;
            }

            var maxObs1 = Number(obs1.maxPoints);
            if (isNaN(maxObs1)) {
                maxObs1 = 0;
            }
            var obs2 = Obstacles.findOne({name: round.obstacle2});
            if (obs2 == undefined) {
                obs2 = {name: "none", description: "none", owner: "none", maxPoints: 0, tickPoints: 0};
                obsTypeOn = obsTypeOn+10;
            }
            var maxObs2 = Number(obs2.maxPoints);
            if (isNaN(maxObs2)) {
                maxObs2 = 0;
            }

            var obs3 = Obstacles.findOne({name: round.obstacle3});
            if (obs3 == undefined) {
                obs3 = {name: "none", description: "none", owner: "none", maxPoints: 0, tickPoints: 0};
                obsTypeOn = obsTypeOn+100;
            }
            var maxObs3 = Number(obs3.maxPoints);
            if (isNaN(maxObs3)) {
                maxObs3 = 0;
            }

            var obs4 = Obstacles.findOne({name: round.obstacle4});
            if (obs4 == undefined) {
                obs4 = {name: "none", description: "none", owner: "none", maxPoints: 0, tickPoints: 0};
                obsTypeOn = obsTypeOn+1000;
            }
            var maxObs4 = Number(obs4.maxPoints);
            if (isNaN(maxObs4)) {
                maxObs4 = 0;
            }

            if (round != null) {
                SideData.update(mySideData._id, {
                    $set: {
                        action:2,
                        roundId: id, roundTime: round.timeLeft,state:round.state,isDone:2,
                        leftSide: {
                            groupName: round.leftSide,
                            newPoints: 0,
                            obs1Points: 0,
                            obs2Points: 0,
                            bonus:0
                        },
                        rightSide: {
                            groupName: round.rightSide,
                            newPoints: 0,
                            obs1Points: 0,
                            obs2Points: 0,
                            bonus: 0
                        },
                        obs1Max: maxObs1, obs1Owner: "none",
                        obs2Max: maxObs2, obs2Owner: "none",
                        obs3Max: maxObs3, obs3Owner: "none",
                        obs4Max: maxObs4, obs4Owner: "none"
                    }
                });
            }
            Rounds.update(round._id,{$set:{isDone:2,executeTime: Date.now()+round.timeLeft*60*1000}});
            var andData = {action:"startRound",data:{_id:id,timeLeft:round.timeLeft,obsType:obsTypeOn,obs1tick:obs1.tickPoints,obs2tick:obs2.tickPoints,obs3tick:obs3.tickPoints,obs4tick:obs4.tickPoints}};
            updateAndroid(andData);
            return round.timeLeft;
        }else{
            return -1;
        }
    },
    showResults: function(newState){
        var myGameState = GameState.find({}).fetch();
        GameState.update(myGameState[0]._id, {$set: {state: newState}});
        var mySideData = SideData.findOne({});
        var myRound = Rounds.findOne({_id:mySideData.roundId});
        if (myRound != null){
            //myRound.isDone = 3;
            //myRound.leftSide.obs1Points = mySideData.leftSide.obs1Points;
            //myRound.leftSide.obs2Points = mySideData.leftSide.obs2Points;
            //myRound.rightSide.obs1Points = mySideData.rightSide.obs1Points;
            //myRound.rightSide.obs2Points = mySideData.rightSide.obs2Points;
            Rounds.update(myRound._id, {$set: {isDone:3}});
        }
    },
    getGameState: function(){
        return GameState.findOne({});
    }

});


function updateAndroid(data){
    var and = AndroidEvents.findOne();
    AndroidEvents.update(and._id,{$set:data});
}




//DONE
Picker.route( '/android/getgroups', function( params, request, response, next ) {
    t = Groups.find().fetch();

    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    response.end(JSON.stringify(t));
});

//Requires app to read it correctly
Picker.route( '/android/getrounds', function( params, request, response, next ) {
    var t = Rounds.find({}).fetch();
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;

    var responseObject = {data:t};
    response.end(JSON.stringify(responseObject));
});





Picker.route( '/android/markrounddone', function( params, request, response, next ) {
    var k = Rounds.findOne({_id:  request.body[0]});
    if (k){
      Rounds.update(k._id,{$set: {progress:request.body[1]}});
      response.end(JSON.stringify({}));
    }
});





//packet request has changed values need to update in app.
// roundId
// adds any data that is required
// phase1[successes]
// phase2{1,2,3,4,5,6,7,8,9[successes]}
// phase3[volt value]
// copter value
// catcher value
// innovability value
// add check if group already has 3 rounds refuse.
// request not done requires app first.
Picker.route( '/android/addround', function( params, request, response, next ) {
    var k = Rounds.find({group_id:  request.body[0], round_type : 0});
    var result_count = k.count();
    if (request.body[2]==0 && result_count >= 3){
      //var t = Rounds.find({}).fetch();
      response.end(JSON.stringify([]));
      return;
    }

    var kk = Rounds.find({group_id:  request.body[0], round_type : 1});
    var result_count2 = kk.count();
    if (request.body[2]==1 && result_count2 >= 1){
      //var t = Rounds.find({}).fetch();
      response.end(JSON.stringify([]));
      return;
    }
    if (request.body[2] != 0){
      result_count = 10;
    }
    addNewRound(request.body[0], request.body[1], request.body[2], result_count);

    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var t = Rounds.find({}).fetch();
    response.end(JSON.stringify(t));
});


function addNewRound(id, time, type, result_count){
  Rounds.insert({group_id:  id, round_time: time, round_type: type, progress:0, round_order : result_count+1},function(err,id){
    if (!err){
      initRoundData(id,type);
    }
  });
}





function initRoundData(id,round_type){
    switch (round_type){
      case 0:{ // field
        RoundParts.insert({round_id:  id, round_part_type: "phase1", value: {data_type : "unk"}});
        RoundParts.insert({round_id:  id, round_part_type: "phase2", value: {data_type : "unk"}});
        RoundParts.insert({round_id:  id, round_part_type: "phase3", value: {data_type : "unk"}});
        RoundParts.insert({round_id:  id, round_part_type: "copter", value: {data_type : "copter"}});
        RoundParts.insert({round_id:  id, round_part_type: "catcher", value: {data_type : "catcher"}});
        RoundParts.insert({round_id:  id, round_part_type: "innovation", value: {data_type : "innovation"}});
        break;
      }
      case 1:{ //comumnity
          RoundParts.insert({round_id:  id, round_part_type: "community_score", value: {data_type : "community_score"}});
          break;
      }
    }
}


Picker.route('/android/deleteround/:_id', function(params, req, res, next) {
    var rp = RoundParts.find({round_id: params._id});

    rp.forEach((roundpart) => {
      RoundParts.remove(roundpart._id);
    });

    var round = Rounds.findOne({_id: params._id});

    if (round){
      var group_id = round.group_id;
      var deleted_round_order = round.round_order;
      var r = Rounds.find({'group_id':group_id, round_type: round.round_type, round_order: {$gt:deleted_round_order}});
      r.forEach((in_round) => {
        //console.log("round updated order to minus 1: " + in_round.round_order);
        var needed_round_or = in_round.round_order-1;
        Rounds.update(in_round._id,{$set: {round_order:needed_round_or}});
      });
    }


    t = Rounds.remove({_id:params._id});
    res.end("k");
});


Picker.route('/android/deletegroup/:_id', function(params, req, res, next) {
    var s = Groups.findOne({_id:params._id});
    var rounds = Rounds.find( { group_id : s._id }).fetch();

    for (var i=0; i<rounds.length ; i++){
        var rp = RoundParts.find({round_id: rounds[i]._id});
        rp.forEach((roundpart) => {
          RoundParts.remove(roundpart._id);
        });
        Rounds.remove({_id:rounds[i]._id});
    }

    t = Groups.remove({_id:params._id});
    res.end("");
});

// 0 round id
// 1 round type
// 2 round part type
// 3 full json data value

Picker.route( '/android/addnewroundpart', function( params, request, response, next ) {
    addRoundPartData(request.body[0],request.body[1],request.body[2],request.body[3]);
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var t = Rounds.find({}).fetch();
    response.end(JSON.stringify(t));
});



// 0 roundpart id
// 1 updated json data
Picker.route( '/android/updateroundpart', function( params, request, response, next ) {
    //console.log("id: "+request.body[0]);
    //console.log("app status: "+request.body[1]);
    //console.log("data: "+request.body[3]);
    if ( is_change_round_values_allowed(request.body[2],request.body[1])){
        updateRoundPartDataByPartId(request.body[0],request.body[3]);
    }
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var t = Rounds.find({}).fetch();
    response.end(JSON.stringify(t));
});


Picker.route( '/android/resetgamepart', function( params, request, response, next ) {
    //console.log("id: "+request.body[0]); // roundpart id
    //console.log("app status: "+request.body[1]); // admin status
    //console.log("data: "+request.body[3]); // roundid
    if (request.body[1] == 0){
      var oldrp = RoundParts.findOne({_id:request.body[0]});
      if (oldrp){
        RoundParts.remove({_id:oldrp._id});
        RoundParts.insert({round_id:  oldrp.round_id, round_part_type: oldrp.round_part_type, value: {data_type : "unk"}});
      }
    }

    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    response.end(JSON.stringify([]));
});





function is_change_round_values_allowed(round_id,app_status){
  if (app_status == 0){
    return true;
  }
  var round = Rounds.findOne({_id:round_id});
  //console.log(JSON.stringify(round));
  if (round){
    if (round.progress == 0 && app_status < 3){
      return true;
    } else{
      return false;
    }
  }
}



Picker.route( '/android/updategrouppoints', function( params, request, response, next ) {
    //console.log("id" + request.body[0]);
    //console.log("points: "+request.body[1]);
    var g = Groups.findOne({_id: request.body[0]});
    if (g){
      Groups.update(request.body[0],{$set: {points:request.body[1]}});
    }
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    response.end("{}");
});



// full json data
function addRoundPartData(id,round_type,round_part_type,data){
  var t = Rounds.find({_id:  id}).fetch();
  delete data._id;
  if (t){
    var s = RoundParts.findOne({'round_part_type':  round_part_type});
    if (s){
      RoundParts.update(s._id, {$set: {value : data}});
    }
  }
}

// only updated json data
function updateRoundPartDataByPartId(id, data){
    var s = RoundParts.findOne({'_id':  id});
    if (s){
      var old_data = s.value;
      if (old_data == null){
        old_data = {};
      }
      var requiresRefresh = true;
      for (var key in data){
          if (data[key] == -1){
            delete data[key];
            delete old_data[key];
            requiresRefresh = false;
          }else{
            old_data[key] = data[key];
          }
      }
      old_data['requiresRefresh'] = requiresRefresh;
      //console.log( "part id: " + s._id);
      console.log( "new data: " + JSON.stringify(old_data));
      RoundParts.update(s._id, {$set: {value : old_data}});
    }
}


//TODO add rounditem


//only admin app should be able to send this request
Picker.route( '/android/addgroup', function( params, request, response, next ) {
        Groups.insert({groupName:  request.body[0], description:request.body[1], number:request.body[2]});
        response.setHeader( 'Content-Type', 'application/json' );
        response.statusCode = 200;
        t = Groups.find().fetch();
        response.end(JSON.stringify(t));
});




//maybe change it to updateRound
Picker.route('/android/addpoints', function(params, req, res, next) {
    // roundId
    // adds any data that is required
    // phase1[successes]
    // phase2{1,2,3,4,5,6,7,8,9[successes]}
    // phase3[volt value]
    // copter value
    // catcher value
    // innovability value


    var groupData = {groupName:req.body.groupName,points:req.body.points};
    var mySideData = SideData.findOne({});
    var secondStage = (mySideData.state>=10);
    if (req.body.roundId == mySideData.roundId){
        if ((mySideData.state>1 && mySideData.state < 4)|| (secondStage && mySideData.isDone < 4)){
            var obsName = req.body.pointChangeId;
            var pointsToAdd = Number(req.body.points);

            var leftSide_obs1Points = Number(mySideData.leftSide.obs1Points);
            if (isNaN(leftSide_obs1Points)) {
                leftSide_obs1Points = 0;
            }

            var leftSide_obs2Points = Number(mySideData.leftSide.obs2Points);
            if (isNaN(leftSide_obs2Points)) {
                leftSide_obs2Points = 0;
            }

            var rightSide_obs1Points = Number(mySideData.rightSide.obs1Points);
            if (isNaN(rightSide_obs1Points)) {
                rightSide_obs1Points = 0;
            }

            var rightSide_obs2Points = Number(mySideData.rightSide.obs2Points);
            if (isNaN(rightSide_obs2Points)) {
                rightSide_obs2Points = 0;
            }

            var leftSide_bonus = Number(mySideData.leftSide.bonus);
            if (isNaN(leftSide_bonus)) {
                leftSide_bonus = 0;
            }

            var rightSide_bonus = Number(mySideData.rightSide.bonus);
            if (isNaN(rightSide_bonus)) {
                rightSide_bonus = 0;
            }




            var obs1conf = mySideData.obs1Max - (pointsToAdd + leftSide_obs1Points);
            var obs2conf = mySideData.obs2Max - (pointsToAdd + leftSide_obs2Points);
            var obs3conf = mySideData.obs3Max - (pointsToAdd + rightSide_obs1Points);
            var obs4conf = mySideData.obs4Max - (pointsToAdd + rightSide_obs2Points);


            var executedIncrease = false;
            if (obsName == "1" && obs1conf >= 0 && (leftSide_obs1Points + pointsToAdd) >= 0) {
                //if ()
                leftSide_obs1Points = leftSide_obs1Points + pointsToAdd;
                if (!secondStage && mySideData.obs1Owner != "none") {
                    Meteor.call('addGroupPoints', {groupName: mySideData.obs1Owner, points: -pointsToAdd});
                }
                if (pointsToAdd != 0){
                    executedIncrease = true;
                }
            } else if (obsName == "2" && obs2conf >= 0 && (leftSide_obs2Points + pointsToAdd) >= 0) {
                leftSide_obs2Points = leftSide_obs2Points + pointsToAdd;
                if (!secondStage && mySideData.obs2Owner != "none") {
                    Meteor.call('addGroupPoints', {groupName: mySideData.obs2Owner, points: -pointsToAdd});
                }
                if (pointsToAdd != 0){
                    executedIncrease = true;
                }
            } else if (obsName == "3" && obs3conf >= 0 && (rightSide_obs1Points + pointsToAdd) >= 0) {
                rightSide_obs1Points = rightSide_obs1Points + pointsToAdd;
                if (!secondStage && mySideData.obs3Owner != "none") {
                    Meteor.call('addGroupPoints', {groupName: mySideData.obs3Owner, points: -pointsToAdd});
                }
                if (pointsToAdd != 0){
                    executedIncrease = true;
                }
            } else if (obsName == "4" && obs4conf >= 0 && (rightSide_obs2Points + pointsToAdd) >= 0) {
                rightSide_obs2Points = rightSide_obs2Points + pointsToAdd;
                if (!secondStage && mySideData.obs4Owner != "none") {
                    Meteor.call('addGroupPoints', {groupName: mySideData.obs4Owner, points: -pointsToAdd});
                }
                if (pointsToAdd != 0){
                    executedIncrease = true;
                }
            } else if (obsName == "5"){
                leftSide_bonus =  leftSide_bonus + pointsToAdd;
				executedIncrease = true;
            } else if (obsName == "6"){
                rightSide_bonus = rightSide_bonus + pointsToAdd;
				executedIncrease = true;
            }

            var lleftSide = mySideData.leftSide;
            var rrightSide = mySideData.rightSide;
            lleftSide.obs1Points = leftSide_obs1Points;
            lleftSide.obs2Points = leftSide_obs2Points;
            lleftSide.bonus = leftSide_bonus;
            rrightSide.obs1Points = rightSide_obs1Points;
            rrightSide.obs2Points = rightSide_obs2Points;
            rrightSide.bonus = rightSide_bonus;



            var Jsonobj = {
                leftSide: lleftSide,
                rightSide: rrightSide,
                action:7
            }
            SideData.update(mySideData._id, {$set: Jsonobj});

            if (executedIncrease) {

                var andData = {action:"pointsUpdate",data:{_id:mySideData.roundId,leftobs1:leftSide_obs1Points,leftobs2:leftSide_obs2Points,rightobs1:rightSide_obs1Points,rightobs2:rightSide_obs2Points,leftBonus: leftSide_bonus,rightBonus: rightSide_bonus}};
                updateAndroid(andData);
                if (!secondStage) {
                    Rounds.update(mySideData.roundId,{$set:{obs1Points:lleftSide.obs1Points,obs2Points:lleftSide.obs2Points,obs3Points:rrightSide.obs1Points,obs4Points:rrightSide.obs2Points,totalLeft:lleftSide.obs1Points+lleftSide.obs2Points+lleftSide.bonus    ,leftBonus: leftSide_bonus,rightBonus: rightSide_bonus     ,totalRight:rrightSide.obs1Points+rrightSide.obs2Points+rrightSide.bonus}});
                    Meteor.call('addGroupPoints', groupData);
                }else{
                    Rounds.update(mySideData.roundId,{$set:{obs1Points:lleftSide.obs1Points,obs2Points:lleftSide.obs2Points,obs3Points:rrightSide.obs1Points,obs4Points:rrightSide.obs2Points,totalLeft:lleftSide.obs1Points+lleftSide.obs2Points+lleftSide.bonus    ,leftBonus: leftSide_bonus,rightBonus: rightSide_bonus     ,totalRight:rrightSide.obs1Points+rrightSide.obs2Points+rrightSide.bonus}});
                    var round = Rounds.findOne({_id:mySideData.roundId});
                    var result = lleftSide.obs1Points+lleftSide.obs2Points+lleftSide.bonus-(rrightSide.obs1Points+rrightSide.obs2Points+rrightSide.bonus);
                    if (result>0){
                        Rounds.update(mySideData.roundId,{$set:{winner:1,isDone:3}});
                        if (round.state == 11){
                            var round2 = Rounds.findOne({state:15});
                            round2.leftSide = round.leftSide;
                            updateRound(round2);
                        }else if (round.state == 12){
                            var round2 = Rounds.findOne({state:15});
                            round2.rightSide = round.leftSide;
                            updateRound(round2);
                        }else if (round.state == 13){
                            var round2 = Rounds.findOne({state:16});
                            round2.leftSide = round.leftSide;
                            updateRound(round2);
                        }else if (round.state == 14){
                            var round2 = Rounds.findOne({state:16});
                            round2.rightSide = round.leftSide;
                            updateRound(round2);
                        }else if (round.state == 15){
                            var round2 = Rounds.findOne({state:18});
                            var round3 = Rounds.findOne({state:17});
                            round2.leftSide = round.leftSide;
                            round3.leftSide = round.rightSide;
                            updateRound(round2);
                            updateRound(round3);
                        }else if (round.state == 16){
                            var round2 = Rounds.findOne({state:18});
                            var round3 = Rounds.findOne({state:17});
                            round2.rightSide = round.leftSide;
                            round3.rightSide = round.rightSide;
                            updateRound(round2);
                            updateRound(round3);
                        }else if (round.state == 17){
                            var finalrank = FinalRanks.findOne({});
                            FinalRanks.update(finalrank._id,{$set:{third:round.leftSide}});
                        }else if (round.state == 18){
                            var finalrank = FinalRanks.findOne({});
                            FinalRanks.update(finalrank._id,{$set:{second:round.rightSide,first:round.leftSide}});
                        }

                    }else if (result<0){
                        Rounds.update(mySideData.roundId,{$set:{winner:2,isDone:3}});
                        if (round.state == 11){
                            var round2 = Rounds.findOne({state:15});
                            round2.leftSide = round.rightSide;
                            updateRound(round2);
                        }else if (round.state == 12){
                            var round2 = Rounds.findOne({state:15});
                            round2.rightSide = round.rightSide;
                            updateRound(round2);
                        }else if (round.state == 13){
                            var round2 = Rounds.findOne({state:16});
                            round2.leftSide = round.rightSide;
                            updateRound(round2);
                        }else if (round.state == 14){
                            var round2 = Rounds.findOne({state:16});
                            round2.rightSide = round.rightSide;
                            updateRound(round2);
                        }else if (round.state == 15){
                            var round2 = Rounds.findOne({state:18});
                            var round3 = Rounds.findOne({state:17});
                            round2.leftSide = round.rightSide;
                            round3.leftSide = round.leftSide;
                            updateRound(round2);
                            updateRound(round3);
                        }else if (round.state == 16){
                            var round2 = Rounds.findOne({state:18});
                            var round3 = Rounds.findOne({state:17});
                            round2.rightSide = round.rightSide;
                            round3.rightSide = round.leftSide;
                            updateRound(round2);
                            updateRound(round3);
                        }else if (round.state == 17){
                            var finalrank = FinalRanks.findOne({});
                            FinalRanks.update(finalrank._id,{$set:{third:round.rightSide}});
                        }else if (round.state == 18){
                            var finalrank = FinalRanks.findOne({});
                            finalrank.second = round.leftSide;
                            finalrank.first = round.rightSide;
                            FinalRanks.update(finalrank._id,{$set:{second:round.leftSide,first:round.rightSide}});
                        }
                    }else{
                        Rounds.update(mySideData.roundId,{$set:{winner:0}});
                        if (round.state == 11){
                            var round2 = Rounds.findOne({state:15});
                            round2.leftSide = "Win #1";
                            updateRound(round2);
                        }else if (round.state == 12){
                            var round2 = Rounds.findOne({state:15});
                            round2.rightSide = "Win #2";
                            updateRound(round2);
                        }else if (round.state == 13){
                            var round2 = Rounds.findOne({state:16});
                            round2.leftSide = "Win #3";
                            updateRound(round2);
                        }else if (round.state == 14){
                            var round2 = Rounds.findOne({state:16});
                            round2.rightSide = "Win #4";
                            updateRound(round2);
                        }else if (round.state == 15){
                            var round2 = Rounds.findOne({state:17});
                            var round3 = Rounds.findOne({state:18});
                            round2.leftSide = "Win Left";
                            round3.leftSide = "Lose Left";
                            updateRound(round2);
                            updateRound(round3);
                        }else if (round.state == 16){
                            var round2 = Rounds.findOne({state:17});
                            var round3 = Rounds.findOne({state:18});
                            round2.rightSide = "Win Right";
                            round3.rightSide = "Lose Right";
                            updateRound(round2);
                            updateRound(round3);
                        }
                    }

                }

                res.end("success");
            } else {
                res.end("failed");
            }
        }else{
            res.end("stateDone");
        }
    }else{
        res.end(mySideData.roundId);}
});

function updateRound(round){
    Rounds.update(round._id,{$set:(round)});
}


Picker.route('/android/updateobstacles', function(params, req, res, next) {
    //console.log("update Obstacles: " + JSON.stringify(req.body));
    var myRound = Rounds.findOne({_id:req.body[0]});
    var sstate = myRound.state;

    if ((sstate == 0) && ((req.body[3] != "none" || req.body[4] != "none") && (req.body[1] != "none" || req.body[2] != "none"))){
        sstate = 1;
        var andData = {action:"statusReady",data:myRound._id};
        updateAndroid(andData);
    }

    Rounds.update(req.body[0],{$set: {obstacle1:req.body[1],obstacle2:req.body[2],obstacle3:req.body[3],obstacle4:req.body[4],state:sstate,isDone:1,leftSide:req.body[5],rightSide:req.body[6],timeLeft:req.body[7]}});
    res.setHeader( 'Content-Type', 'application/json' );
    res.statusCode = 200;
    //var state = Meteor.call('getGameState').state;
    var t = Rounds.find({}).fetch();
    res.end(JSON.stringify(t));
});


Picker.route('/android/updateround', function(params, req, res, next) {
    Rounds.update(req.body[0],{$set: {leftSide:req.body[1],rightSide:req.body[2],timeLeft:Number(req.body[3])}});
    res.setHeader( 'Content-Type', 'application/json' );
    res.statusCode = 200;
    var t = Rounds.find({}).fetch();
    res.end(JSON.stringify(t));

});


Picker.route('/android/updateroundref', function(params, req, res, next) {
    if (req.body[1] < 3 && req.body[0] != null){
      //console.log(req.body[0]);
      Rounds.update(req.body[0],{$set: {referee_name:req.body[2]}});
    }
    res.setHeader( 'Content-Type', 'application/json' );
    res.statusCode = 200;
    res.end(JSON.stringify([req.body[2]]));
});

Picker.route('/android/updateroundpause', function(params, req, res, next) {
    if (req.body[1] < 3 && req.body[0] != null){
      //console.log(req.body[0]);
      if (req.body[2] == -1){
          Rounds.update(req.body[0],{$set: {pause_left:-1,progress:0}});
      }else{
          Rounds.update(req.body[0],{$set: {pause_left:req.body[2],progress:2}});
      }
    }
    res.setHeader( 'Content-Type', 'application/json' );
    res.statusCode = 200;
    res.end(JSON.stringify([req.body[2]]));
});

Picker.route( '/android/setgamestage', function( params, request, response, next ) {
    Meteor.call('changeGameStage', request.body[0]);
    var stage = RoundsOrder.findOne({stage:request.body[0]});
    if (stage.type == 0){
        Meteor.call('changeGameState', "groupList");
        t = Rounds.find({}).fetch(); //state: { $lte: 9}
        response.setHeader( 'Content-Type', 'application/json' );
        response.statusCode = 200;
        response.end(JSON.stringify(t));
    }else{
        Meteor.call('changeGameState', "playoffList");
        t = Rounds.find({}).fetch();
        response.setHeader( 'Content-Type', 'application/json' );
        response.statusCode = 200;
        response.end(JSON.stringify(t));
    }
});


Picker.route( '/android/calculateplayoffs', function( params, request, response, next ) {
    var roundorder = RoundsOrder.findOne({stage:request.body[0],type:1});
    if (roundorder != undefined) {
        Rounds.remove({state: { $gt: 10},stage: request.body[0]});
        var new_order = [];
        var items = Groups.find({}, {sort: {points: -1}});
        var itemF = items.fetch();

        if (itemF.length<4){
            response.setHeader('Content-Type', 'application/json');
            response.statusCode = 200;
            var t = ["need atleast 4 groups"];
            response.end(JSON.stringify(t));
            finish = true;
        }else {
            var duration = roundorder.roundDuration;
            var moreIds = Rounds.batchInsert([
                {
                    leftSide: itemF[0].groupName,
                    rightSide: "none",
                    executeTime: -1,
                    timeLeft: duration,
                    state: 11,
                    winner: 0,
                    isDone: 0,
                    stage: roundorder.stage,
                    obs1Points: 0,
                    obs2Points: 0,
                    obs3Points: 0,
                    obs4Points: 0,
                    leftBonus: 0,
                    rightBonus: 0
                }
                , {
                    leftSide: itemF[1].groupName,
                    rightSide: "none",
                    executeTime: -1,
                    timeLeft: duration,
                    state: 12,
                    winner: 0,
                    isDone: 0,
                    stage: roundorder.stage,
                    obs1Points: 0,
                    obs2Points: 0,
                    obs3Points: 0,
                    obs4Points: 0,
                    leftBonus: 0,
                    rightBonus: 0
                }
                , {
                    leftSide: itemF[2].groupName,
                    rightSide: "none",
                    executeTime: -1,
                    timeLeft: duration,
                    state: 13,
                    winner: 0,
                    isDone: 0,
                    stage: roundorder.stage,
                    obs1Points: 0,
                    obs2Points: 0,
                    obs3Points: 0,
                    obs4Points: 0,
                    leftBonus: 0,
                    rightBonus: 0
                }
                , {
                    leftSide: itemF[3].groupName,
                    rightSide: "none",
                    executeTime: -1,
                    timeLeft: duration,
                    state: 14,
                    winner: 0,
                    isDone: 0,
                    stage: roundorder.stage,
                    obs1Points: 0,
                    obs2Points: 0,
                    obs3Points: 0,
                    obs4Points: 0,
                    leftBonus: 0,
                    rightBonus: 0
                }
                , {
                    leftSide: "Win #1",
                    rightSide: "Win #2",
                    executeTime: -1,
                    timeLeft: duration,
                    state: 15,
                    winner: 0,
                    isDone: 0,
                    stage: roundorder.stage,
                    obs1Points: 0,
                    obs2Points: 0,
                    obs3Points: 0,
                    obs4Points: 0,
                    leftBonus: 0,
                    rightBonus: 0
                }
                , {
                    leftSide: "Win #3",
                    rightSide: "Win #4",
                    executeTime: -1,
                    timeLeft: duration,
                    state: 16,
                    winner: 0,
                    isDone: 0,
                    stage: roundorder.stage,
                    obs1Points: 0,
                    obs2Points: 0,
                    obs3Points: 0,
                    obs4Points: 0,
                    leftBonus: 0,
                    rightBonus: 0
                }
                , {
                    leftSide: "Lose #3",
                    rightSide: "Lose #4",
                    executeTime: -1,
                    timeLeft: duration,
                    state: 17,
                    winner: 0,
                    isDone: 0,
                    stage: roundorder.stage,
                    obs1Points: 0,
                    obs2Points: 0,
                    obs3Points: 0,
                    obs4Points: 0,
                    leftBonus: 0,
                    rightBonus: 0
                }
                , {
                    leftSide: "Win Left",
                    rightSide: "Win Right",
                    executeTime: -1,
                    timeLeft: duration,
                    state: 18,
                    winner: 0,
                    isDone: 0,
                    stage: roundorder.stage,
                    obs1Points: 0,
                    obs2Points: 0,
                    obs3Points: 0,
                    obs4Points: 0,
                    leftBonus: 0,
                    rightBonus: 0
                }
            ], function (err, res) {
                RoundsOrder.update(roundorder._id, {$set: {order: res}});
                t = Rounds.find({}).fetch();
                response.setHeader('Content-Type', 'application/json');
                response.statusCode = 200;
                response.end(JSON.stringify(t));
                finish = true;
            });
        }
    }else{
        response.setHeader('Content-Type', 'application/json');
        response.statusCode = 200;
        var t = ["This is not playoff stage!"];
        response.end(JSON.stringify(t));
        finish = true;
    }
})



Picker.route( '/android/show_winners', function( params, request, response, next ) {
    var state = Meteor.call('getGameState').state;
    Meteor.call('changeGameState', "winnerList");
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    response.end("success");
});


Picker.route( '/android/approve_round', function( params, request, response, next ) {
    var state = Meteor.call('getGameState').state;
    if (state == "playoffMatchList" || state == "playoffList"){
        Meteor.call('changeGameState', "playoffList");
        Meteor.call('setPlayoffRoundStateDone', request.body[0]);
    }else if (state == "resultList" || state == "groupList"){
        //Meteor.call('changeGameState', "groupList");
        Meteor.call('setRoundStateDone', request.body[0]);
    }
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var jsonArrayResponse = [];
    jsonArrayResponse.push("success");
    response.end(JSON.stringify(jsonArrayResponse));
});



Picker.route( '/android/orderchange', function( params, request, response, next ) {
    var temp = request.body;
    var stage = request.body[0];
    temp.splice(0, 1);
    var rounds_order = RoundsOrder.findOne({stage:stage});
    RoundsOrder.update(rounds_order._id,{$set:{order:temp}});
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var jsonArrayResponse = [];
    jsonArrayResponse.push("success");
    response.end(JSON.stringify(jsonArrayResponse));
});



Picker.route( '/android/reviewround', function( params, request, response, next ) {
    var id = request.body[0];
    Meteor.call('setRoundStateReview', id);
    Meteor.call('showResults', "resultList");

    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var jsonArrayResponse = [];
    jsonArrayResponse.push("success");
    response.end(JSON.stringify(jsonArrayResponse));
});





Picker.route( '/android/resetround', function( params, request, response, next ) {
    var roundId = request.body[0];
    var theRound = Rounds.findOne({_id:roundId});
    if (theRound){
        var state = theRound.state;
        if (state < 5){
            if (state >=2) {
                if (theRound.obstacle1 != "none") {
                    var obs1 = Obstacles.findOne({name: theRound.obstacle1});
                    if (obs1.owner != "none") {
                        var owner1GainWas = obs1.maxPoints - theRound.obs1Points;
                        Meteor.call('addGroupPoints', {groupName: obs1.owner, points: -owner1GainWas});
                    }
                }
                if (theRound.obstacle2 != "none") {
                    var obs2 = Obstacles.findOne({name: theRound.obstacle2});
                    if (obs2.owner != "none") {
                        var owner2GainWas = obs2.maxPoints - theRound.obs2Points;
                        Meteor.call('addGroupPoints', {groupName: obs2.owner, points: -owner2GainWas});
                    }
                }
                if (theRound.obstacle3 != "none") {
                    var obs3 = Obstacles.findOne({name: theRound.obstacle3});
                    if (obs3.owner != "none") {
                        var owner3GainWas = obs3.maxPoints - theRound.obs3Points;
                        Meteor.call('addGroupPoints', {groupName: obs3.owner, points: -owner3GainWas});
                    }
                }
                if (theRound.obstacle4 != "none") {
                    var obs4 = Obstacles.findOne({name: theRound.obstacle4});
                    if (obs4.owner != "none") {
                        var owner4GainWas = obs4.maxPoints - theRound.obs4Points;
                        Meteor.call('addGroupPoints', {groupName: obs4.owner, points: -owner4GainWas});
                    }
                }
            }
            state = 1;

            Meteor.call('addGroupPoints', {groupName: theRound.leftSide, points: -theRound.totalLeft});
            Meteor.call('addGroupPoints', {groupName: theRound.rightSide, points: -theRound.totalRight});
        }


        Rounds.update(roundId,{$set:{isDone:1,state:state,obs1Points:0,obs2Points:0,obs3Points:0,obs4Points:0,totalLeft:0,totalRight:0,leftBonus:0,rightBonus:0}});

        var mySideData = SideData.findOne({});
        if (roundId == mySideData.roundId){
            mySideData.roundId = 0;
            SideData.update(mySideData._id,{$set:mySideData});
            var state = Meteor.call('getGameState').state;
            if (state == "playoffMatchList" || state == "playoffList"){
                Meteor.call('changeGameState', "playoffList");
            }else if (state == "resultList" || state == "groupList" || state == "matchList"){
                Meteor.call('changeGameState', "groupList");
            }
        }

        var andData = {action:"statusReady",data:roundId};
        updateAndroid(andData);
        //var andData = {action:"pointsUpdate",data:{_id:roundId,leftobs1:0,leftobs2:0,rightobs1:0,rightobs2:0,leftBonus: 0,rightBonus: 0}};
        //updateAndroid(andData);


        response.setHeader( 'Content-Type', 'application/json' );
        response.statusCode = 200;
        var jsonArrayResponse = [];
        jsonArrayResponse.push("success");
        response.end(JSON.stringify(jsonArrayResponse));
    }else{
        response.setHeader( 'Content-Type', 'application/json' );
        response.statusCode = 200;
        var jsonArrayResponse = [];
        jsonArrayResponse.push("didnt find round by id");
        response.end(JSON.stringify(jsonArrayResponse));
    }
});


function passRoundDataToSideData(roundId){
    var mySideData = SideData.findOne({});
    var myRound = Rounds.findOne({_id:roundId});

    SideData.update(mySideData._id, {
        $set: {
            action:1,
            roundId: myRound._id, roundTime: myRound.timeLeft,state:myRound.state,isDone:myRound.isDone,
            leftSide: {
                groupName: myRound.leftSide,
                newPoints: 0,
                obs1Points: 0,
                obs2Points: 0
            },
            rightSide: {
                groupName: myRound.rightSide,
                newPoints: 0,
                obs1Points: 0,
                obs2Points: 0
            },
            obs1Max: maxObs1, obs1Owner: obs1.owner,
            obs2Max: maxObs2, obs2Owner: obs2.owner,
            obs3Max: maxObs3, obs3Owner: obs3.owner,
            obs4Max: maxObs4, obs4Owner: obs4.owner
        }
    });
}




Picker.route( '/android/addrawpoints', function( params, request, response, next ) {
    var groupId = request.body[0];
    var points = Number(request.body[1]);
    var g = Groups.findOne({_id: groupId});
    if (g){
      new_admin_extra = g.admin_extra;
      if (!isNaN(new_admin_extra)){
          new_admin_extra = new_admin_extra + points;
      }else{
          new_admin_extra = points;
      }

      Groups.update(g._id,{$set:{admin_extra:new_admin_extra}});

    }

    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var jsonArrayResponse = [];
    jsonArrayResponse.push("success");
    response.end(JSON.stringify(jsonArrayResponse));
});




Picker.route( '/android/create_backup', function( params, request, response, next ) {
    var backupDBGroups = Groups.find({}).fetch();
    var backupDBRounds = Rounds.find({}).fetch();
    var backupDBRoundParts = RoundParts.find({}).fetch();

    for(var i = 0; i < backupDBGroups.length; i++) {
        backupDBGroups[i]['old_id'] = backupDBGroups[i]['_id'];
        delete backupDBGroups[i]['_id'];
        delete backupDBGroups[i]['points'];
        delete backupDBGroups[i]['admin_extra'];
    }
    var newBackupDBRounds = [];
    for(var i = 0; i < backupDBRounds.length; i++) {
        newBackupDBRounds[i] = {};
        newBackupDBRounds[i]['old_id'] = backupDBRounds[i]['_id'];
        //delete backupDBRounds[i]['_id'];
        newBackupDBRounds[i]['progress'] = 0;
        newBackupDBRounds[i]['group_id'] = backupDBRounds[i]['group_id'];
        newBackupDBRounds[i]['round_time'] = backupDBRounds[i]['round_time'];
        newBackupDBRounds[i]['round_type'] = backupDBRounds[i]['round_type'];
        newBackupDBRounds[i]['round_order'] = backupDBRounds[i]['round_order'];
        //delete backupDBRounds[i]['referee_name'];
        //delete backupDBRounds[i]['pause_left'];

    }

    for(var i = 0; i < backupDBRoundParts.length; i++) {
        delete backupDBRoundParts[i]['_id'];
        backupDBRoundParts[i]['progress'] = 0;
    }

    Backups.remove({});
    Backups.insert({groups :backupDBGroups, rounds: newBackupDBRounds, roundparts: backupDBRoundParts}, function (err, res) {
        var jsonArrayResponse = [];
        jsonArrayResponse.push("success");
        if (err){
            jsonArrayResponse.push("failed");
        }
        response.setHeader( 'Content-Type', 'application/json' );
        response.statusCode = 200;
        response.end(JSON.stringify(jsonArrayResponse));
    });
});



Picker.route( '/android/restore_backup', function( params, request, response, next ) {
    var backup = Backups.findOne({});
    var items = 0;
    //console.log(JSON.stringify(backup.stages));

    Groups.remove({});
    Groups.batchInsert(backup.groups,function(err,res){
        if(!err) {
            items = items + 1;
        }
    });

    Obstacles.remove({});
    Obstacles.batchInsert(backup.obstacles,function(err,res){
        if(!err) {
            items = items + 1;
        }
    });
    RoundsOrder.remove({});
    RoundsOrder.batchInsert(backup.stages,function(err,res){
        if(!err) {
            items = items + 1;
        }
    });
    Rounds.remove({});
    SideData.remove({});
    SideData.insert({leftSide:{groupName:"team_test1"},rightSide:{groupName:"team_test3"}, action:0,isDone:0});
    AndroidEvents.remove({});
    AndroidEvents.insert({action:"init",data:{}});
    FinalRanks.remove({});
    FinalRanks.insert({first: "none", second:"none", third:"none"});
    GameState.remove({});
    GameState.insert({state:"groupList", gameStage:1});

    var jsonArrayResponse = [];
    jsonArrayResponse.push("success");
    if (items < 3){
        jsonArrayResponse.push("failed");
    }
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    response.end(JSON.stringify(jsonArrayResponse));
});



var backup;

Picker.route( '/android/restore_backup_device', function( params, request, response, next ) {
    var bk_data = request.body[0];
    backup = JSON.parse(bk_data);
    var items = 0;
    //console.log(JSON.stringify(backup.stages));


    Groups.remove({});
    Groups.batchInsert(backup.groups,function(err,res){
        if(!err) {
            items = items + 1;
        }
    });

    Obstacles.remove({});
    Obstacles.batchInsert(backup.obstacles,function(err,res){
        if(!err) {
            items = items + 1;
        }
    });


    Rounds.remove({});
    Rounds.batchInsert(backup.rounds,function(err,res){
        if(!err) {
            items = items + 1;
        }
    });

    Meteor.setTimeout(function(){
        for (t=0;t<backup.stages.length;t++){
            var oldIds = backup.stages[t]['old_order'];
            for (j=0;j<oldIds.length;j++){
                var tempRound = Rounds.findOne({old_id:oldIds[j]});
                if (tempRound != null){
                    backup.stages[t]['order'].push(tempRound._id);
                }
            }
        }
    },5000);

    Meteor.setTimeout(function(){
        RoundsOrder.remove({});
        RoundsOrder.batchInsert(backup.stages,function(err,res){
        });
    },6000);



    SideData.remove({});
    SideData.insert({leftSide:{groupName:"team_test1"},rightSide:{groupName:"team_test3"}, action:0,isDone:0});
    AndroidEvents.remove({});
    AndroidEvents.insert({action:"init",data:{}});
    FinalRanks.remove({});
    FinalRanks.insert({first: "none", second:"none", third:"none"});
    GameState.remove({});
    GameState.insert({state:"groupList", gameStage:1});

    var jsonArrayResponse = [];
    jsonArrayResponse.push("success");
    if (items < 3){
        jsonArrayResponse.push("failed");
    }
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    response.end(JSON.stringify(jsonArrayResponse));
});



function resetRound(theRound){
    var state = theRound.state;
    var isdone = theRound.isDone;
    var roundId = theRound._id;
    if (state > 0 && state < 5){
        state = 1;
        isdone = 1;
    }



    Rounds.update(roundId,{$set:{isDone:isdone,state:state,obs1Points:0,obs2Points:0,obs3Points:0,obs4Points:0,totalLeft:0,totalRight:0,leftBonus:0,rightBonus:0}});
    //var andData = {action:"statusReady",data:roundId};
    //updateAndroid(andData);
    //var andData = {action:"pointsUpdate",data:{_id:roundId,leftobs1:0,leftobs2:0,rightobs1:0,rightobs2:0,leftBonus: 0,rightBonus: 0}};
    //updateAndroid(andData);
}

function resetGroup(theGroup){
    Groups.update(theGroup._id,{$set:{points:0}});
}


Picker.route( '/android/reset_group_points', function( params, request, response, next ) {
    //Meteor.call('changeGameState', "groupList");
    var groups = Groups.find({}).fetch();
    for (i=0;i<groups.length;i++){
        resetGroup(groups[i]);
    }

    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var jsonArrayResponse = [];
    jsonArrayResponse.push("success");
    response.end(JSON.stringify(jsonArrayResponse));
});



var backupTimestamp = 0;
Picker.route( '/android/reset_season', function( params, request, response, next ) {
    var d = new Date();
    var n = d.getTime();
    if (n > backupTimestamp+30000){
      backupTimestamp = n;
      var backup = Backups.findOne({});
      if (backup){
        RoundParts.remove({});
        Rounds.remove({});
        Groups.remove({});
        var groups = backup.groups;
        var rounds = backup.rounds;
        var roundparts = backup.roundparts;

        for (i=0;i<groups.length;i++){
            Groups.insert({groupName:  groups[i].groupName, description:groups[i].description, number:groups[i].number, old_id: groups[i].old_id});
        }

        db_groups = Groups.find({}).fetch();
        for (i=0;i<db_groups.length;i++){
            var oid = db_groups[i]["old_id"];
            for (j=0;j<rounds.length;j++){
                if (rounds[j]["group_id"] == oid){
                    rounds[j]["group_id"] = db_groups[i]._id;

                }
            }
        }
        console.log("rounds length: "+ rounds.length);
        for (i=0;i<rounds.length;i++){
            console.log("i: " + i);
            addNewRound(rounds[i].group_id, rounds[i].round_time, rounds[i].round_type, rounds[i].round_order-1);
        }
      }
    }else{
      console.log("executed too fast so ignore");
    }
    response.setHeader( 'Content-Type', 'application/json' );
    response.statusCode = 200;
    var jsonArrayResponse = [];
    jsonArrayResponse.push("success");
    response.end(JSON.stringify(jsonArrayResponse));
});
