/**
 * Created by Amir on 1/7/2017.
 */
Meteor.publish('groups', function() {
    return Groups.find({});
});

Meteor.publish('rounds', function() {
    return Rounds.find({});
});

Meteor.publish('roundparts', function() {
    return RoundParts.find({});
});
